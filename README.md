# This is the official Creative ModPack endorsed by the Athion Project

The pack utilises both Forge and LiteLoader to load the following mods:
  - DaFlight
  - WorldEditCUI
  - Optifine
  - Shaders
  - Xaero's Minimap
  - PixelCam

## Disclaimer

 The mods in this pack are liable to change due to version outdating and better alternatives being
 found.

### Liability

 Utilizing this ModPack means that you agree that we are not liable with for damage to your computer or Minecraft installation encountered during the use of this ModPack, nor do we claim any rights to the mods themselves with full credit going to the mod makers.

 We are operating and composing this ModPack under the assumption that if the binaries required these mods are provided to the public for public use.  If you disagree with how we've presented one of your mods please be sure to contact us directly and we will remove your mod in lieu of finding a better solution.

### Data Collection and Profiling

 We collect absolutely no data on your usage of this ModPack what so ever.  Granted data collection does help improve the quality of software, but we believe in your privacy and freedom and will not be tracking anything.  This includes your Minecraft authentication we will have no part in stealing passwords and usernames.  

## Installation [Pending Changes to a OneClick solution]

   1. Extract the contents of this folder into a destination of your choosing
   2. Run the file "Forge.jar" file and select "Install Client", making sure it runs correctly
   3. Run the "LiteLoader.Jar" file
   4. Select "Install LiteLoader (recommended)"
   5. Click on box next to "Extend from:" and select "1.8-Forge11.14.1.1334"
   6. Name the "New Profile" to your liking
   7. Check the minecraft directory and click "OK", making sure it runs correctly
   8. Navigate to your ".minecraft" folder *[navigation]*
   9. Copy and paste, or drag, the "mods" folder into your ".minecraft" folder
   10. Launch minecraft and select your new profile. If no name was set it will be "LiteLoader 1.8 with Forge 11.14.1.1334"
   11. Enjoy!

## Crediting Mod Creators

#### [Forge](http://www.minecraftforge.net/forum/)
#### [LiteLoader](http://www.liteloader.com/)
#### [DaFlight](http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1293938-daflight-fly-mod-v2-4r9)
#### [WorldEditCUI](http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1292886-worldeditcui)
#### [Optifine](http://optifine.net/home)
#### [Shaders](http://www.shadersmod.net/)
#### [Xaero's Minimap](http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2379024-xaeros-minimap)
#### [MCUpdater](http://mcupdater.com/)
#### [PixelCam] (http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2327429-pixelcam-camera-studio-for-minecraft-1-8)
